﻿using AutoMapper;
using BLL.Dto;
using BLL.Infrastructure;
using BLL.Interfaces;
using LibraryTaskMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LibraryTaskMVC.Controllers
{
    public class GuestController : Controller
    {
        readonly IFeedBackServiceAsync feedBackService;
        public GuestController(IFeedBackServiceAsync service)
        {
            feedBackService = service;
        }
        public async Task<ActionResult> Index()
        {
            var feedBacks = await feedBackService.GetAllAsync();
            var config = new MapperConfiguration(cfg => cfg.CreateMap<FeedBackDto, FeedBackViewModel>());
            var mapper = new Mapper(config);
            IEnumerable<FeedBackViewModel> result = mapper.Map<IEnumerable<FeedBackDto>, IEnumerable<FeedBackViewModel>>(feedBacks);
            ViewData["FeedBacks"] = result;
            return View();
        }
        public async Task<ActionResult> Details (int id)
        {
            var feedBack = await feedBackService.GetByIdAsync(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<FeedBackDto, FeedBackViewModel>());
            var mapper = new Mapper(config);
            var result = mapper.Map<FeedBackDto, FeedBackViewModel>(feedBack);
            return View(result);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FeedBackViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("Index");
                }
                var config = new MapperConfiguration(cfg => cfg.CreateMap<FeedBackViewModel, FeedBackDto>());
                var mapper = new Mapper(config);
                FeedBackDto item = mapper.Map<FeedBackViewModel, FeedBackDto>(viewModel);
                await feedBackService.CreateAsync(item);
                return RedirectToAction("Index");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return RedirectToAction("Index");
        }
        public async Task<ActionResult> Edit(int id)
        {
            var feedBack = await feedBackService.GetByIdAsync(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<FeedBackDto, FeedBackViewModel>());
            var mapper = new Mapper(config);
            var result = mapper.Map<FeedBackDto, FeedBackViewModel>(feedBack);
            return View(result);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, FeedBackViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("Index");
                }
                var config = new MapperConfiguration(cfg => cfg.CreateMap<FeedBackViewModel, FeedBackDto>());
                var mapper = new Mapper(config);
                FeedBackDto item = mapper.Map<FeedBackViewModel, FeedBackDto>(viewModel);
                item.FeedBackId = id;
                await feedBackService.UpdateAsync(item);
                return RedirectToAction("Index");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                return View();
            }
        }
        // GET: Goods/Delete/5
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var feedBack = await feedBackService.GetByIdAsync(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<FeedBackDto, FeedBackViewModel>());
            var mapper = new Mapper(config);
            var result = mapper.Map<FeedBackDto, FeedBackViewModel>(feedBack);
            return View(result);
        }

        // POST: Goods/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, FeedBackViewModel viewModel)
        {
            try
            {
                await feedBackService.DeleteAsync(id);
                return RedirectToAction("Index");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                return View();
            }
        }
    }
}
