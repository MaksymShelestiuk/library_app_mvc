﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LibraryTaskMVC.Models;
using AutoMapper;
using BLL.Dto;
using BLL.Infrastructure;

namespace LibraryTaskMVC.Controllers
{
    public class MainController : Controller
    {
        readonly IArticleServiceAsync articleService;
        public MainController (IArticleServiceAsync aservice)
        {
            articleService = aservice;
        }
        public async Task<ActionResult> Index()
        {
            var articles = await articleService.GetAllAsync();
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDto, ArticleViewModel>());
            var mapper = new Mapper(config);
            IEnumerable<ArticleViewModel> result = mapper.Map<IEnumerable<ArticleDto>, IEnumerable<ArticleViewModel>>(articles);
            return View(result);
        }
        public async Task<ActionResult> Details(int id)
        {
            var article = await articleService.GetByIdAsync(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDto, ArticleViewModel>());
            var mapper = new Mapper(config);
            var result = mapper.Map<ArticleDto, ArticleViewModel>(article);
            return View(result);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ArticleViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("Index");
                }
                var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleViewModel, ArticleDto>());
                var mapper = new Mapper(config);
                ArticleDto item = mapper.Map<ArticleViewModel, ArticleDto>(viewModel);
                await articleService.CreateAsync(item);
                return RedirectToAction("Index");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
            }
            return RedirectToAction("Index");
        }
        public async Task<ActionResult> Edit(int id)
        {
            var feedBack = await articleService.GetByIdAsync(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDto, ArticleViewModel>());
            var mapper = new Mapper(config);
            var result = mapper.Map<ArticleDto, ArticleViewModel>(feedBack);
            return View(result);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, ArticleViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("Index");
                }
                var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleViewModel, ArticleDto>());
                var mapper = new Mapper(config);
                ArticleDto item = mapper.Map<ArticleViewModel, ArticleDto>(viewModel);
                item.ArticleId = id;
                await articleService.UpdateAsync(item);
                return RedirectToAction("Index");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                return View();
            }
        }
        // GET: Goods/Delete/5
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            var feedBack = await articleService.GetByIdAsync(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDto, ArticleViewModel>());
            var mapper = new Mapper(config);
            var result = mapper.Map<ArticleDto, ArticleViewModel>(feedBack);
            return View(result);
        }

        // POST: Goods/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, ArticleViewModel viewModel)
        {
            try
            {
                await articleService.DeleteAsync(id);
                return RedirectToAction("Index");
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError(ex.Property, ex.Message);
                return View();
            }
        }
    }
}
