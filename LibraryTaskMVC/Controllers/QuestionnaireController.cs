﻿using AutoMapper;
using BLL.Dto;
using BLL.Interfaces;
using LibraryTaskMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LibraryTaskMVC.Controllers
{
    public class QuestionnaireController : Controller
    {
        readonly IArticleServiceAsync articleService;
        readonly ITagServiceAsync tagService;
        readonly IQuestionnaireService questionnaireService;
        public QuestionnaireController(IArticleServiceAsync aservice, IQuestionnaireService qservice, ITagServiceAsync tservice)
        {
            articleService = aservice;
            questionnaireService = qservice;
            tagService = tservice;
        }
        public async Task<ActionResult> Index()
        {
            SelectList items = new SelectList(await tagService.GetAllAsync(), "Name", "Name");
            ViewBag.Tags = items;
            return View();
        }
        public async Task<ActionResult> SearchResult(string title, string author, string sortdate)
        {
            return View(await questionnaireService.SearchArticle(title, author, sortdate));
        }
        public async Task<ActionResult> ViewArticle(int id)
        {
            var article = await articleService.GetByIdAsync(id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDto, ArticleViewModel>());
            var mapper = new Mapper(config);
            ArticleViewModel vm = mapper.Map<ArticleDto, ArticleViewModel>(article);
            ViewBag.Article = vm;
            return View("SearchResult");
        }
        public async Task<ActionResult> GetAllArticles()
        {
            var articles = await articleService.GetAllAsync();
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDto, ArticleViewModel>());
            var mapper = new Mapper(config);
            IEnumerable<ArticleViewModel> result = mapper.Map<IEnumerable<ArticleDto>, IEnumerable<ArticleViewModel>>(articles);
            return View("SearchResult", result);
        }

        [HttpPost]
        public ActionResult SearchArticle(string title, string author, string sortdate)
        {
            return RedirectToAction("SearchResult", new {title=title, author=author, sortdate=sortdate});
        }
    }
}
