﻿using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryTaskMVC
{
    public class DependencyResolverModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IArticleServiceAsync>().To<ArticleService>();
            Bind<ITagServiceAsync>().To<TagService>();
            Bind<IFeedBackServiceAsync>().To<FeedBackService>();
            Bind<IUserProfileServiceAsync>().To<UserProfileService>();
            Bind<IQuestionnaireService>().To<QuestionnaireService>();
        }
    }
}