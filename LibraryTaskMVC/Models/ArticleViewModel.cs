﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryTaskMVC.Models
{
    public class ArticleViewModel
    {
        public int ArticleId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int UserProfileId { get; set; }
        public DateTime DateOfPublication { get; set; }
        public string Content { get; set; }
        public string TagName { get; set; }
        public int TagId { get; set; }
    }
}