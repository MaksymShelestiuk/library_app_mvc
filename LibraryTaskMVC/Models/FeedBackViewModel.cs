﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryTaskMVC.Models
{
    public class FeedBackViewModel
    {
        public int FeedBackId { get; set; }
        public string Author { get; set; }
        public int UserProfileId { get; set; }
        public DateTime DateOfReview { get; set; }
        public string ReviewContent { get; set; }
    }
}