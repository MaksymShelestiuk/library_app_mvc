﻿using LibraryTaskMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LibraryTaskMVC.Extensions
{
    public static class HtmlHelperForArticle
    {
        public static IHtmlString ArticleList(this HtmlHelper<IEnumerable<ArticleViewModel>> htmlHelper, IEnumerable<ArticleViewModel> viewModel)
        {
            StringBuilder htmlBuilder = new StringBuilder();
            var outerOl = new TagBuilder("ol");
            foreach (ArticleViewModel article in viewModel)
            {
                var li = new TagBuilder("li");
                var table = new TagBuilder("table");
                table.AddCssClass("table");
                var tr = new TagBuilder("tr");
                var td1 = new TagBuilder("td");
                td1.SetInnerText($"Title: {article.Title}");
                var td2 = new TagBuilder("td");
                td2.SetInnerText($"Author: {article.Author}");
                var td3 = new TagBuilder("td");
                td3.SetInnerText($"Date Of Publication: {article.DateOfPublication}");
                var td4 = new TagBuilder("td");
                td4.SetInnerText($"Tag: {article.TagName}");
                var td5 = new TagBuilder("td");
                var button = new TagBuilder("input");
                button.MergeAttribute("type", "button");
                button.MergeAttribute("value", "Open");
                button.MergeAttribute("onclick", $"location.href='/Questionnaire/ViewArticle/{article.ArticleId}'");
                htmlBuilder.Append(li.ToString(TagRenderMode.StartTag));
                htmlBuilder.Append(table.ToString(TagRenderMode.StartTag));
                htmlBuilder.Append(tr.ToString(TagRenderMode.StartTag));
                htmlBuilder.Append(td1.ToString(TagRenderMode.Normal));
                htmlBuilder.Append(td2.ToString(TagRenderMode.Normal));
                htmlBuilder.Append(td3.ToString(TagRenderMode.Normal));
                htmlBuilder.Append(td4.ToString(TagRenderMode.Normal));
                htmlBuilder.Append(td5.ToString(TagRenderMode.StartTag));
                htmlBuilder.Append(button.ToString(TagRenderMode.Normal));
                htmlBuilder.Append(td5.ToString(TagRenderMode.EndTag));
                htmlBuilder.Append(tr.ToString(TagRenderMode.EndTag));
                htmlBuilder.Append(table.ToString(TagRenderMode.EndTag));
                htmlBuilder.Append(li.ToString(TagRenderMode.EndTag));
            }
            outerOl.InnerHtml = htmlBuilder.ToString();
            var html = outerOl.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(html);
        }
    }
}