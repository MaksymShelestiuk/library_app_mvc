﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArticleTitleMaxLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Article", "Title", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Article", "Title", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
