﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tag", "Name", c => c.String(nullable: false, maxLength: 12));
            AlterColumn("dbo.FeedBack", "ReviewContent", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FeedBack", "ReviewContent", c => c.String());
            AlterColumn("dbo.Tag", "Name", c => c.String());
        }
    }
}
