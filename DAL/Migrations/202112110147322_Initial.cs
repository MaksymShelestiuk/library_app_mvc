﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Article",
                c => new
                    {
                        ArticleId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 20),
                        UserProfileId = c.Int(nullable: false),
                        DateOfPublication = c.DateTime(nullable: false),
                        Content = c.String(),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ArticleId)
                .ForeignKey("dbo.Tag", t => t.TagId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileId, cascadeDelete: true)
                .Index(t => t.UserProfileId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.TagId);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserProfileId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        SecondName = c.String(),
                    })
                .PrimaryKey(t => t.UserProfileId);
            
            CreateTable(
                "dbo.FeedBack",
                c => new
                    {
                        FeedBackId = c.Int(nullable: false, identity: true),
                        UserProfileId = c.Int(nullable: false),
                        DateOfReview = c.DateTime(nullable: false),
                        ReviewContent = c.String(),
                    })
                .PrimaryKey(t => t.FeedBackId)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileId, cascadeDelete: true)
                .Index(t => t.UserProfileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FeedBack", "UserProfileId", "dbo.UserProfile");
            DropForeignKey("dbo.Article", "UserProfileId", "dbo.UserProfile");
            DropForeignKey("dbo.Article", "TagId", "dbo.Tag");
            DropIndex("dbo.FeedBack", new[] { "UserProfileId" });
            DropIndex("dbo.Article", new[] { "TagId" });
            DropIndex("dbo.Article", new[] { "UserProfileId" });
            DropTable("dbo.FeedBack");
            DropTable("dbo.UserProfile");
            DropTable("dbo.Tag");
            DropTable("dbo.Article");
        }
    }
}
