﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class FeedBack
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FeedBackId { get; set; }
        [Required]
        [ForeignKey(nameof(UserProfile))]
        public int UserProfileId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        [Required]
        public DateTime DateOfReview { get; set; }
        [Required]
        [MinLength(2)]
        public string ReviewContent { get; set; }
    }
}
