﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserProfileRepository : IRepository<UserProfile>
    {
        private readonly LibraryTaskContext _db;
        public UserProfileRepository(LibraryTaskContext context)
        {
            _db = context;
        }
        public async Task<UserProfile> CreateAsync(UserProfile item)
        {
            _db.UserProfiles.Add(item);
            await _db.SaveChangesAsync();
            return item;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _db.UserProfiles.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _db.UserProfiles.Remove(item);
                await _db.SaveChangesAsync();
            }
            return result;
        }

        public IEnumerable<UserProfile> Find(Func<UserProfile, Boolean> predicate)
        {
            return _db.UserProfiles.Where(predicate).ToList();
        }

        public async Task<IEnumerable<UserProfile>> GetAllAsync()
        {
            return await _db.UserProfiles.ToListAsync();
        }

        public async Task<UserProfile> GetByIdAsync(int id)
        {
            return await _db.UserProfiles.FindAsync(id);
        }

        public async Task<bool> UpdateAsync(UserProfile item)
        {
            var entity = _db.UserProfiles.Attach(item);
            _db.Entry(item).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return entity != null;
        }
    }
}
