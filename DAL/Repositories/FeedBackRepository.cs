﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class FeedBackRepository : IRepository<FeedBack>
    {
        private readonly LibraryTaskContext _db;
        public FeedBackRepository(LibraryTaskContext context)
        {
            _db = context;
        }
        public async Task<FeedBack> CreateAsync(FeedBack item)
        {
            _db.FeedBacks.Add(item);
            await _db.SaveChangesAsync();
            return item;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _db.FeedBacks.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _db.FeedBacks.Remove(item);
                await _db.SaveChangesAsync();
            }
            return result;
        }

        public IEnumerable<FeedBack> Find(Func<FeedBack, Boolean> predicate)
        {
            return _db.FeedBacks.Where(predicate).ToList();
        }

        public async Task<IEnumerable<FeedBack>> GetAllAsync()
        {
            return await _db.FeedBacks.ToListAsync();
        }

        public async Task<FeedBack> GetByIdAsync(int id)
        {
            return await _db.FeedBacks.FindAsync(id);
        }

        public async Task<bool> UpdateAsync(FeedBack item)
        {
            var entity = _db.FeedBacks.Attach(item);
            _db.Entry(item).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return entity != null;
        }
    }
}
