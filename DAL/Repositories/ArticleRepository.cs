﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private readonly LibraryTaskContext _db;
        public ArticleRepository(LibraryTaskContext context)
        {
            _db = context;
        }
        public async Task<Article> CreateAsync(Article item)
        {
            _db.Articles.Add(item);
            await _db.SaveChangesAsync();
            return item;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _db.Articles.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _db.Articles.Remove(item);
                await _db.SaveChangesAsync();
            }
            return result;
        }

        public IEnumerable<Article> Find(Func<Article, Boolean> predicate)
        {
            return _db.Articles.Where(predicate).ToList();
        }

        public async Task<IEnumerable<Article>> GetAllAsync()
        {
            return await _db.Articles.ToListAsync();
        }

        public async Task<Article> GetByIdAsync(int id)
        {
            return await _db.Articles.FindAsync(id);
        }

        public async Task<bool> UpdateAsync(Article item)
        {
            var entity = _db.Articles.Attach(item);
            _db.Entry(item).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return entity != null;
        }
    }
}
