﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TagRepository : IRepository<Tag>
    {
        private readonly LibraryTaskContext _db;
        public TagRepository(LibraryTaskContext context)
        {
            _db = context;
        }
        public async Task<Tag> CreateAsync(Tag item)
        {
            _db.Tags.Add(item);
            await _db.SaveChangesAsync();
            return item;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _db.Tags.FindAsync(id);
            var result = item != null;
            if (result)
            {
                _db.Tags.Remove(item);
                await _db.SaveChangesAsync();
            }
            return result;
        }

        public IEnumerable<Tag> Find(Func<Tag, Boolean> predicate)
        {
            return _db.Tags.Where(predicate).ToList();
        }

        public async Task<IEnumerable<Tag>> GetAllAsync()
        {
            return await _db.Tags.ToListAsync();
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await _db.Tags.FindAsync(id);
        }

        public async Task<bool> UpdateAsync(Tag item)
        {
            var entity = _db.Tags.Attach(item);
            _db.Entry(item).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return entity != null;
        }
    }
}
