﻿using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryTaskContext _db;
        private ArticleRepository _articleRepository;
        private TagRepository _tagRepository;
        private FeedBackRepository _feedBackRepository;
        private UserProfileRepository _userProfileRepository;
        public UnitOfWork()
        {
            _db = new LibraryTaskContext();
        }
        public IRepository<Article> Articles
        {
            get
            {
                if (_articleRepository == null)
                    _articleRepository = new ArticleRepository(_db);
                return _articleRepository;
            }
        }

        public IRepository<Tag> Tags
        {
            get
            {
                if (_tagRepository == null)
                    _tagRepository = new TagRepository(_db);
                return _tagRepository;
            }
        }
        public IRepository<FeedBack> FeedBacks
        {
            get
            {
                if (_feedBackRepository == null)
                    _feedBackRepository = new FeedBackRepository(_db);
                return _feedBackRepository;
            }
        }
        public IRepository<UserProfile> UserProfiles
        {
            get
            {
                if (_userProfileRepository == null)
                    _userProfileRepository = new UserProfileRepository(_db);
                return _userProfileRepository;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
