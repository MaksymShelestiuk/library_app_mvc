﻿using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Article> Articles { get; }
        IRepository<Tag> Tags { get; }
        IRepository<FeedBack> FeedBacks { get; }
        IRepository<UserProfile> UserProfiles { get; }
        void Save();
    }
}
