﻿using DAL.UoW;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;

namespace BLL.Services
{
    public class UserProfileService : IUserProfileServiceAsync
    {
        IUnitOfWork Db { get; set; }
        public UserProfileService(IUnitOfWork uow)
        {
            Db = uow;
        }
        public async Task<UserProfile> CreateAsync(UserProfile item)
        {
            if (!IsStringValid(item.FirstName) && !IsStringValid(item.SecondName))
            {
                throw new ArgumentException();
            }
            else
                return await Db.UserProfiles.CreateAsync(item);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await Db.UserProfiles.DeleteAsync(id);
        }

        public async Task<IEnumerable<UserProfile>> GetAllAsync()
        {
            return await Db.UserProfiles.GetAllAsync();
        }

        public async Task<UserProfile> GetByIdAsync(int id)
        {
            return await Db.UserProfiles.GetByIdAsync(id);
        }

        public async Task<bool> UpdateAsync(UserProfile item)
        {
            return await Db.UserProfiles.UpdateAsync(item);
        }

        public IEnumerable<UserProfile> Find(Func<UserProfile, bool> predicate)
        {
            return Db.UserProfiles.Find(predicate);
        }

        public bool IsStringValid(string text)
        {
            bool isletter = true;
            int i = 0;
            while (isletter && i<text.Length)
            {
                isletter = char.IsLetter(text[i]);
                i++;
            }
            return text.Length > 0 && isletter && char.IsUpper(text[0]);
        }
    }

}
