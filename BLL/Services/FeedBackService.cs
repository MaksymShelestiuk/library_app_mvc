﻿using DAL.UoW;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Dto;
using BLL.Extensions;
using BLL.Infrastructure;

namespace BLL.Services
{
    public class FeedBackService : IFeedBackServiceAsync
    {
        IUnitOfWork Db { get; set; }
        public FeedBackService(IUnitOfWork uow)
        {
            Db = uow;
        }
        public async Task<FeedBack> CreateAsync(FeedBackDto item)
        {
            if (!(item is null))
            {
                var author = await Db.UserProfiles.GetByIdAsync(item.UserProfileId);
                if (!(author is null))
                {
                    return await Db.FeedBacks.CreateAsync(item.DtoAsFeedBack());
                }
                else
                    throw new ValidationException("Author of FeedBack doesn't exist", "");
            }
            else
                throw new ValidationException("FeedBack object is null", "");
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await Db.FeedBacks.DeleteAsync(id);
        }

        public async Task<IEnumerable<FeedBackDto>> GetAllAsync()
        {
            List<FeedBackDto> result = new List<FeedBackDto>();
            var items = await Db.FeedBacks.GetAllAsync();
            foreach (FeedBack f in items)
            {
                result.Add(f.FeedBackAsDto(await Db.UserProfiles.GetByIdAsync(f.UserProfileId)));
            }
            return result;
        }

        public async Task<FeedBackDto> GetByIdAsync(int? id)
        {
            if (id == null)
            {
                throw new ValidationException("Id of FeedBack is not set", "");
            }
            var item = await Db.FeedBacks.GetByIdAsync(id.Value);
            if (item == null)
            {
                throw new ValidationException("FeedBack was not found", "");
            }
            return item.FeedBackAsDto(await Db.UserProfiles.GetByIdAsync(item.UserProfileId));
        }

        public async Task<bool> UpdateAsync(FeedBackDto item)
        {
            return await Db.FeedBacks.UpdateAsync(item.DtoAsFeedBack());
        }

        public async Task<IEnumerable<FeedBackDto>> Find(Func<FeedBack, bool> predicate)
        {
            List<FeedBackDto> result = new List<FeedBackDto>();
            var items = Db.FeedBacks.Find(predicate);
            foreach (FeedBack f in items)
            {
                result.Add(f.FeedBackAsDto(await Db.UserProfiles.GetByIdAsync(f.UserProfileId)));
            }
            return result;
        }
    }
}
