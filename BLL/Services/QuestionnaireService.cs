﻿using BLL.Dto;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class QuestionnaireService : IQuestionnaireService
    {
        private readonly IArticleServiceAsync articleService;
        readonly IUserProfileServiceAsync userProfileService;
        public QuestionnaireService(IArticleServiceAsync aservice, IUserProfileServiceAsync uservice)
        {
            articleService = aservice;
            userProfileService = uservice;
        }
        /// <summary>
        /// Searches Articles using Author`s FirstName or SecondName and using Title of article
        /// Also sorts found Articles by the Date of Publication 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="author"></param>
        /// <param name="sortdate"></param>
        /// <returns>Articles if found</returns>
        public async Task<IEnumerable<ArticleDto>> SearchArticle(string title, string author, string sortdate)
        {
            IEnumerable<ArticleDto> result;
            List<ArticleDto> articles = new List<ArticleDto>();
            if (!String.IsNullOrEmpty(author) && !String.IsNullOrEmpty(title))
            {
                articles = await Search(author, title);
            }
            else if (!String.IsNullOrEmpty(title))
            {
                articles.AddRange(await articleService.Find(article => article.Title.Contains(title)));
            }
            else if (!String.IsNullOrEmpty(author))
            {
                articles = await Search(author);
            }
            result = articles;
            if (sortdate != null)
            {
                if (sortdate == "asc")
                {
                    result = articles.OrderBy(a => a.DateOfPublication);
                }
                else
                {
                    result = articles.OrderByDescending(a => a.DateOfPublication);
                }
            }
            return result;
        }
        /// <summary>
        /// Searches Articles using Author`s FirstName or SecondName 
        /// </summary>
        /// <param name="author"></param>
        /// <returns>List of Articles</returns>
        private async Task<List<ArticleDto>> Search(string author)
        {
            List<ArticleDto> result = new List<ArticleDto>();
            if (!String.IsNullOrEmpty(author))
            {
                var authors = userProfileService.Find(u => u.FirstName.Contains(author) || u.SecondName.Contains(author));
                foreach (UserProfile a in authors)
                {
                    result.AddRange(await articleService.Find(article => article.UserProfileId == a.UserProfileId));
                }
            }
            return result;
        }
        /// <summary>
        /// Searches Articles using Author`s FirstName or SecondName and using Title of article
        /// </summary>
        /// <param name="author"></param>
        /// <param name="title"></param>
        /// <returns>List of Articles</returns>
        private async Task<List<ArticleDto>> Search(string author, string title)
        {
            List<ArticleDto> result = new List<ArticleDto>();
            if (!String.IsNullOrEmpty(author) && !String.IsNullOrEmpty(title))
            {
                var authors = userProfileService.Find(u => u.FirstName.Contains(author) || u.SecondName.Contains(author));
                foreach (UserProfile a in authors)
                {
                    result.AddRange(await articleService.Find(article => 
                    article.UserProfileId == a.UserProfileId && article.Title.Contains(title)));
                }
            }
            return result;
        }
    }
}
