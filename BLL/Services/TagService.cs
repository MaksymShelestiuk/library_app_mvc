﻿using DAL.UoW;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;

namespace BLL.Services
{
    public class TagService : ITagServiceAsync
    {
        IUnitOfWork Db { get; set; }
        public TagService(IUnitOfWork uow)
        {
            Db = uow;
        }
        public async Task<Tag> CreateAsync(Tag item)
        {
            if (!IsStringValid(item.Name))
            {
                throw new ArgumentException();
            }
            else
                return await Db.Tags.CreateAsync(item);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await Db.Tags.DeleteAsync(id);
        }

        public async Task<IEnumerable<Tag>> GetAllAsync()
        {
            return await Db.Tags.GetAllAsync();
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            return await Db.Tags.GetByIdAsync(id);
        }

        public async Task<bool> UpdateAsync(Tag item)
        {
            return await Db.Tags.UpdateAsync(item);
        }

        public IEnumerable<Tag> Find(Func<Tag, bool> predicate)
        {
            return Db.Tags.Find(predicate);
        }

        public bool IsStringValid(string text)
        {
            return text.Length > 0 && char.IsLetter(text[0]) && char.IsUpper(text[0]);
        }
    }

}

