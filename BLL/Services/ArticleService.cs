﻿using DAL.UoW;
using DAL.Models;
using BLL.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Infrastructure;
using BLL.Dto;

namespace BLL.Services
{
    public class ArticleService : IArticleServiceAsync
    {
        IUnitOfWork Db { get; set; }
        public ArticleService(IUnitOfWork uow)
        {
            Db = uow;
        }
        public async Task<Article> CreateAsync(ArticleDto article)
        {
            if (!(article is null))
            {
                var author = await Db.UserProfiles.GetByIdAsync(article.UserProfileId);
                if (!(author is null))
                {
                    var tag = await Db.Tags.GetByIdAsync(article.TagId);
                    if (!(tag is null))
                    {
                        if (!IsStringValid(article.Title))
                        {
                            throw new ValidationException("Title of Article is not Valid", "");
                        }
                        else
                            return await Db.Articles.CreateAsync(article.DtoAsArticle());
                    }
                    else
                        throw new ValidationException("Tag of Article doesn't exist", "");
                }
                else
                    throw new ValidationException("Author of Article doesn't exist", "");
            }
            else
                throw new ValidationException("Article object is null", "");
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await Db.Articles.DeleteAsync(id);
        }

        public async Task<IEnumerable<ArticleDto>> GetAllAsync()
        {
            List<ArticleDto> result = new List<ArticleDto>();
            var articles = await Db.Articles.GetAllAsync();
            foreach (Article a in articles)
            {
                result.Add(a.ArticleAsDto(await Db.UserProfiles.GetByIdAsync(a.UserProfileId), await Db.Tags.GetByIdAsync(a.TagId)));
            }
            return result;
        }
        public async Task<ArticleDto> GetByIdAsync(int? id)
        {
            if (id == null)
            {
                throw new ValidationException("Id of Article is not set", "");
            }
            var article = await Db.Articles.GetByIdAsync(id.Value);
            if (article == null)
            {
                throw new ValidationException("Article was not found", "");
            }
            return article.ArticleAsDto(await Db.UserProfiles.GetByIdAsync(article.UserProfileId), await Db.Tags.GetByIdAsync(article.TagId));
        }

        public async Task<bool> UpdateAsync(ArticleDto item)
        {
            return await Db.Articles.UpdateAsync(item.DtoAsArticle());
        }

        public async Task<IEnumerable<ArticleDto>> Find(Func<Article, bool> predicate)
        {
            List<ArticleDto> result = new List<ArticleDto>();
            var articles = Db.Articles.Find(predicate);
            foreach (Article a in articles)
            {
                result.Add(a.ArticleAsDto(await Db.UserProfiles.GetByIdAsync(a.UserProfileId), await Db.Tags.GetByIdAsync(a.TagId)));
            }
            return result;
        }

        public bool IsStringValid(string text)
        {
            return text.Length > 0 && char.IsLetter(text[0]) && char.IsUpper(text[0]);
        }
    }

}