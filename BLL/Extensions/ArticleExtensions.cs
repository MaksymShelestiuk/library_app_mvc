﻿using BLL.Dto;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Extensions
{
    public static class ArticleExtensions
    {
        public static ArticleDto ArticleAsDto(this Article item, UserProfile user, Tag tag)
        {
            return new ArticleDto
            {
                ArticleId = item.ArticleId,
                Title = item.Title,
                Author = user.FirstName+" "+user.SecondName,
                UserProfileId = user.UserProfileId,
                DateOfPublication = item.DateOfPublication,
                Content = item.Content,
                TagName = tag.Name,
                TagId = tag.TagId
            };
        }
        public static Article DtoAsArticle(this ArticleDto item)
        {
            return new Article
            {
                ArticleId = item.ArticleId,
                Title = item.Title,
                UserProfileId = item.UserProfileId,
                DateOfPublication = item.DateOfPublication,
                Content = item.Content,
                TagId = item.TagId
            };
        }
    }
}
