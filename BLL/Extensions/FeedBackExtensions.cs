﻿using BLL.Dto;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Extensions
{
    public static class FeedBackExtensions
    {
        public static FeedBackDto FeedBackAsDto(this FeedBack item, UserProfile user)
        {
            return new FeedBackDto
            {
                FeedBackId = item.FeedBackId,
                Author = user.FirstName + " " + user.SecondName,
                UserProfileId = item.UserProfileId,
                DateOfReview = item.DateOfReview,
                ReviewContent = item.ReviewContent
            };
        }
        public static FeedBack DtoAsFeedBack(this FeedBackDto item)
        {
            return new FeedBack
            {
                FeedBackId = item.FeedBackId,
                UserProfileId = item.UserProfileId,
                DateOfReview = item.DateOfReview,
                ReviewContent = item.ReviewContent
            };
        }
    }
}
