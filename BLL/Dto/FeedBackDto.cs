﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Dto
{
    public class FeedBackDto
    {
        public int FeedBackId { get; set; }
        public string Author { get; set; }
        public int UserProfileId { get; set; }
        public DateTime DateOfReview { get; set; }
        public string ReviewContent { get; set; }
    }
}
