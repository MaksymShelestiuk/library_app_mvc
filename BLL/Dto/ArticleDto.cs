﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Dto
{
    public class ArticleDto
    {
        public int ArticleId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int UserProfileId { get; set; }
        public DateTime DateOfPublication { get; set; }
        public string Content { get; set; }
        public string TagName { get; set; }
        public int TagId { get; set; }
    }
}
