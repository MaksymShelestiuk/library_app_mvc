﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICrudServiceAsync<T> where T : class
    {
        Task<T> CreateAsync(T item);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task<bool> UpdateAsync(T item);
        IEnumerable<T> Find(Func<T, Boolean> predicate);
    }
}
