﻿using BLL.Dto;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IQuestionnaireService
    {
        Task<IEnumerable<ArticleDto>> SearchArticle(string title, string author, string sortdate);
    }
}
