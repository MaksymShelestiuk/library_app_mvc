﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICrudServiceDtoAsync<T, TDto> where T : class where TDto : class
    {
        Task<T> CreateAsync(TDto item);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<TDto>> GetAllAsync();
        Task<TDto> GetByIdAsync(int? id);
        Task<bool> UpdateAsync(TDto item);
        Task<IEnumerable<TDto>> Find(Func<T, bool> predicate);
    }
}
