# Library_app_MVC



## Task1
## Low Level
    1. Create a ASP.NET MVC type (.NET Framework) web-application using a monolithic architecture. Web-application is a prototype of the electronic system "Library".
    2. Appearance design requirements: Style the site according to the theme.
    2.1. The layout should consist of the following components: Title - the full width of the window and it should contain the site name. Insert a graphic site logo in the left corner of the title.
    2.2. The main block is content. It occupies 80% of the width of the window, located in the center. It consists of 2 columns:
    • Menu (right-sidebar)
    • News preview feed (Main-body).
    2.3. Footer – the entire width of the window. It contains copyright and developer information (name, surname, email), as well as a link to the top of the page.
    3. Develop three pages (different controllers):
    3.1. Main - consists of articles (title of the article, date of publication, text of the article) and menu (contains links to other pages - Main, Guest and Questionnaire).
    3.2. Guest - consists of a feed of feedback and a form for providing feedback.
    • The feedback contains the name of the author, the date of the review, the text of the review.
    • The response form contains fields for entering the author's name and text response.
    3.3. Questionnaire - should contain text fields, elements of multiple choice (checkbox), switches. To display list data, create your own Inline Helpers to generate list items (<ul>, <ol>). The questionnaire is processed for GET and POST requests by the method of action with the same name. After the survey, the user is redirected to a new page, which displays the results of the survey.
    4. Only HTML5, CSS3 and JS are allowed when developing the template and presentation.
    5. Upload the project.

## Middle Level:
    1. Implement the Low level.
    2. Develop and use extension method to work with the questionnaire instead of an inline helper.
    3. Bootstrap is mandatory when developing a template and presentation.
    4. Upload the project.
    
## Advanced Level:
    1. Implement the Middle level.
    2. Develop and use both inline helper and extension method to work with the questionnaire.
    3. Use Bootstrap during template development and presentation.
    4. Upload the project.

## Task2
## Low Level
1. Develop classes in accordance with the principles of SOLID, to address DI based on the essence of the subject area of the web application of the prototype of the electronic system "Library" develop classes in accordance with the principles of SOLID, to address DI based on the subject area essence of the web application of the electronic system "Library" prototype (from MVC Task 1) 
2. The names of classes and methods should reflect their functionality.
3. Classes and methods should be structured (folders, namespaces) according to the recommended structure of the MVC project.
4. Code design must comply with C # Code Conventions.
5. The architecture of the web application must conform to the MVC template.
6. Implement server-side data validation at the power and model levels.
7. The code should contain comments where necessary.
8. Upload the project.

## Middle Level:
1. Implement the Low level.
2. Store information about the subject area in the database. Use the Entity Framework and the Code First approach to access the database.
3. Use Microsoft SQL Server as a DBMS.
4. The controllers should not create and directly work with the database context.
5. Add data validation on the customer side.
6. Add autodocumentation.
7. Upload the project to the repository named.

## Advanced Level:
1. Implement the Middle level.
2. Implement a multi-level architecture.
3. The level of access to data must be taken out in a separate project.
4. Add remote data validation.
5. Add modular testing.
6. Use Ninject IoC container to implement DI.
7. Upload the project
